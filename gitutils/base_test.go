package gitutils_test

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/go-git/go-git/v5"
	. "gitlab.com/singh-gur/gitver/gitutils"
)

func TestOpenRepository(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    *git.Repository
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := OpenRepository(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("OpenRepository() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("OpenRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTags(t *testing.T) {
	type args struct {
		repo *git.Repository
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetTags(tt.args.repo)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTags() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSortVersions(t *testing.T) {
	versions := []string{"ver0.0.2", "0.0.1", "0.0.2-beta", "0.0.2-rc1", "0.0.2-alpha"}
	sortedVers := SortVersions(versions)
	fmt.Println(sortedVers)
}

func TestGetVersion(t *testing.T) {
	repo, err := OpenRepository("/home/gurbakhshish/dev/repos/sikhibooks/api_v2")

	if err != nil {
		t.Error(err.Error())
	}

	ver, err := GetVersion(repo)

	if err != nil {
		t.Error(err.Error())
	}

	fmt.Printf("%v", ver.String())
}
