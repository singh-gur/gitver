package gitutils

import (
	"sort"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/hashicorp/go-version"
)

func OpenRepository(path string) (*git.Repository, error) {
	return git.PlainOpen(path)
}

func GetTags(repo *git.Repository) ([]string, error) {
	resp := []string{}
	iter, err := repo.Tags()

	if err != nil {
		return nil, err
	}

	iter.ForEach(func(r *plumbing.Reference) error {
		resp = append(resp, r.Name().Short())
		return nil
	})

	return resp, err
}

func SortVersions(versionsRaw []string) []*version.Version {
	versions := make([]*version.Version, len(versionsRaw))

	for i, raw := range versionsRaw {
		v, _ := version.NewVersion(raw)
		if v != nil {
			versions[i] = v
		}
	}
	sort.Sort(version.Collection(versions))
	return versions
}

func GetVersion(repo *git.Repository) (*version.Version, error) {
	tags, err := GetTags(repo)

	if err != nil {
		return nil, err
	}

	sortedVersions := SortVersions(tags)

	if len(sortedVersions) > 0 {
		return sortedVersions[len(sortedVersions)-1], nil
	}
	return nil, nil
}
