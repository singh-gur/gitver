OUT_DIR ?= ./dist
OS ?= linux
ARCH ?= amd64

build:
	@GOOS=$(OS) \
	GOARCH=$(ARCH) \
	go build \
    -o ${OUT_DIR}/$(OS)/$(ARCH)/gitver \
    -trimpath \
    .
update-deps:
	@go get -u ./...
	@go mod tidy
	@go mod verify