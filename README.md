# GitVer

## How to install?

```go install gitlab.com/singh-gur/gitver@latest```

## How to use it?
This is a simple util that lists git tags as versions and updates them to next versions for my go apps. I may have overengineered it and might have gotten away with a simple shell script.

```
Simple cli to list and update git versions

Usage:
  gitver [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  list        List version
  update      Update version

Flags:
  -h, --help          help for gitver
  -r, --repo string   Repository path (defaults to cwd)

Use "gitver [command] --help" for more information about a command.
```
