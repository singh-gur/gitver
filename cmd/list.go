/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/singh-gur/gitver/gitutils"
)

var (
	withPrefix bool
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List version",
	Long: `List the current version of git repo. For example:
		gitver --repo /path/to/repo list
	`,
	Run: RunList,
}

func RunList(cmd *cobra.Command, args []string) {
	if repoPath == "" {
		cwd, err := os.Getwd()

		if err != nil {
			log.Fatal("Unable to get cwd")
			os.Exit(1)
		}
		repoPath = cwd
	}
	repo, err := gitutils.OpenRepository(repoPath)

	if err != nil {
		log.Fatalf("invalid repo path: %v", err.Error())
	}

	version, err := gitutils.GetVersion(repo)

	if err != nil {
		log.Fatalf("unable to retrieve version: %v", err.Error())
	}

	if version != nil {

		if withPrefix {
			fmt.Print(version.Original())
		} else {
			fmt.Print(version.String())
		}
	}
}

func init() {
	rootCmd.AddCommand(listCmd)
	listCmd.Flags().BoolVarP(&withPrefix, "prefix", "p", true, "preserve 'v' prefix if exists")
}
