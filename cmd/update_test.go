/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd_test

import (
	"strings"
	"testing"

	"github.com/hashicorp/go-version"
	. "gitlab.com/singh-gur/gitver/cmd"
)

// TestNextVersion tests the NextVersion function
func TestNextVersion(t *testing.T) {
	v, _ := version.NewVersion("1.2.3-rc1")
	newVersion, _ := NextVersion(v, "major", "")
	if !strings.Contains(newVersion.String(), "2.0.0") {
		t.Errorf("Expected 2.0.0, got %v", newVersion.String())
	}
	newVersion, _ = NextVersion(v, "minor", "")
	if !strings.Contains(newVersion.String(), "1.3.0") {
		t.Errorf("Expected 1.3.0, got %v", newVersion.String())
	}
	newVersion, _ = NextVersion(v, "patch", "")
	if !strings.Contains(newVersion.String(), "1.2.4") {
		t.Errorf("Expected 1.2.4, got %v", newVersion.String())
	}
	newVersion, _ = NextVersion(v, "build", "")
	if !strings.Contains(newVersion.String(), "1.2.3.1") {
		t.Errorf("Expected 1.2.3.1, got %v", newVersion.String())
	}
	newVersion, _ = NextVersion(v, "", "rc")
	if !strings.Contains(newVersion.String(), "1.2.3-rc2") {
		t.Errorf("Expected 1.2.3-rc1, got %v", newVersion.String())
	}

	newVersion, _ = NextVersion(v, "", "beta")
	if !strings.Contains(newVersion.String(), "1.2.3-beta1") {
		t.Errorf("Expected 1.2.3-rc1, got %v", newVersion.String())
	}
}
