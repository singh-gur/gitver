/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/hashicorp/go-version"
	"github.com/spf13/cobra"
	"gitlab.com/singh-gur/gitver/gitutils"
)

var (
	action string
	pre    string
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update version",
	Long: `Update the current version of git repo. For example:
		gitver --repo /path/to/repo update [options]
	`,
	Run: RunUpdate,
}

func RunUpdate(cmd *cobra.Command, args []string) {

	if action == "" && pre == "" {
		log.Fatal("Pre-release is required if action is not set")
	}

	if !Contains([]string{"major", "minor", "patch", "build", ""}, action) {
		log.Fatal("Invalid/Missing update target, possible options, major,minor,patch,build, this can be left empty if pre flag is set")
	}

	if repoPath == "" {
		cwd, err := os.Getwd()

		if err != nil {
			log.Fatal("Unable to get cwd")
		}
		repoPath = cwd
	}
	repo, err := gitutils.OpenRepository(repoPath)

	if err != nil {
		log.Fatalf("invalid repo path: %v", err.Error())
	}

	version, err := gitutils.GetVersion(repo)

	if err != nil {
		log.Fatalf("unable to retrieve version: %v", err.Error())
	}

	if version != nil {
		newVersion, err := NextVersion(version, action, pre)

		if err != nil {
			log.Fatalf("Unable to get next version: %v", err.Error())
		}

		fmt.Print(newVersion.Original())

	}
}

func NextVersion(v *version.Version, action string, pre string) (nv *version.Version, err error) {

	var prefix string

	if strings.HasPrefix(v.Original(), "v") {
		prefix = "v"
	}

	segments := v.Segments()
	segmentSize := len(segments)
	var newSegments = make([]int, 4)
	nextPre := ""
	switch action {
	case "major":
		major, _ := PopFirst(segments)
		newSegments[0] = major + 1
		segmentSize = Greater(segmentSize, 1)
	case "minor":
		newSegments[0], segments = PopFirst(segments)
		minor, _ := PopFirst(segments)
		newSegments[1] = minor + 1
		segmentSize = Greater(segmentSize, 2)
	case "patch":
		newSegments[0], segments = PopFirst(segments)
		newSegments[1], segments = PopFirst(segments)
		patch, _ := PopFirst(segments)
		newSegments[2] = patch + 1
		segmentSize = Greater(segmentSize, 3)
	case "build":
		newSegments[0], segments = PopFirst(segments)
		newSegments[1], segments = PopFirst(segments)
		newSegments[2], segments = PopFirst(segments)
		build, _ := PopFirst(segments)
		segmentSize = Greater(segmentSize, 4)
		newSegments[3] = build + 1
	case "":
		newSegments = segments
	default:
		return nil, fmt.Errorf("invalid action: %v", action)
	}

	if pre != "" {
		currPre := v.Prerelease()
		nextPre, err = NextPrerelease(currPre, pre, "-")
		if err != nil {
			return nil, err
		}
	}

	strSegments := ConvertIntSlice(newSegments)
	return version.NewVersion(fmt.Sprintf("%v%v%v", prefix, strings.Join(strSegments[:segmentSize], "."), nextPre))
}

func Greater(i int, j int) int {
	if i > j {
		return i
	}
	return j
}

func ZeroVal[T comparable]() T {
	var zero T
	return zero
}

func PopFirst[T comparable](slice []T) (T, []T) {
	if len(slice) == 0 {
		return ZeroVal[T](), slice
	}

	return slice[0], slice[1:]
}

func Contains[T comparable](slice []T, val T) bool {
	if val == ZeroVal[T]() {
		return false
	}

	for _, v := range slice {
		if v == val {
			return true
		}
	}
	return false
}

func ConvertIntSlice(slice []int) []string {
	resp := make([]string, len(slice))
	for i, val := range slice {
		resp[i] = fmt.Sprintf("%v", val)
	}
	return resp
}

func NextPrerelease(preString string, preType string, prefix string) (string, error) {
	if strings.HasPrefix(preString, preType) {
		preVer, err := strconv.Atoi(strings.ReplaceAll(preString, preType, ""))
		if err != nil {
			return "", err
		}
		return fmt.Sprintf("%v%v%v", prefix, preType, preVer+1), nil
	} else {
		return fmt.Sprintf("%v%v1", prefix, preType), nil
	}
}

func init() {
	updateCmd.Flags().StringVarP(&action, "action", "a", "", "Action to perform, major,minor,patch,build")
	updateCmd.Flags().StringVarP(&pre, "pre", "p", "", "Pre-release version")
	rootCmd.AddCommand(updateCmd)
}
