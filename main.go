/*
Copyright © 2023 Gurbakhshish Singh <gurbakhshish.s@gmail.com>
*/
package main

import "gitlab.com/singh-gur/gitver/cmd"

func main() {
	cmd.Execute()
}
